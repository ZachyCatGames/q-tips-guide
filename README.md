If you are looking for the official build of Switchroot Android Q, go here: https://forum.xda-developers.com/t/rom-unofficial-10-switchroot-android-10.4229761/

By compiling yourself, you may be using untested changes/features that may cause undesired problems or even cause damage to your device, continue at your own risk

# Clean Build:
Follow https://wiki.lineageos.org/devices/foster/build upto "Prepare the device-specific code"  

Make sure to init with "lineage-17.1", not "lineage-15.1" or anything else during "Initialize the LineageOS source repository"
```bash
git clone https://gitlab.com/switchroot/android/manifest.git -b lineage-17.1-icosa_sr .repo/local_manifests
.repo/local_manifests/snack.sh -y
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G
```
Run one of the following:

`lunch lineage_icosa_sr-userdebug`: Standard Android with Nvidia apps.  
`lunch lineage_icosa_tv_sr-userdebug`: Android TV with Nvidia apps.  
NOTE: Downloadable Nvidia Shield exclusive games/ports (such as Half Life 2) do not currently work.  

Now run:
```bash
make bacon
```

Download hekate and yeet it's contents on your SD card.

Put https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/00-android.ini?inline=false in `/bootloader/ini` on your SD card.

Place coreboot.rom in `/switchroot/android/` on your SD card:  
https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/coreboot.rom or https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/coreboot_oc.rom if you want a 1862MHz memory overclock (rename to coreboot.rom. NOTE: You'll have to recopy the OC coreboot after flashing a lineage zip/updating if you want to keep ram OC)

Place `common.scr` and `sd.scr` on your SD card in `/switchroot/android/` and rename `sd.scr` to `boot.scr`  
https://gitlab.com/switchroot/bootstack/switch-uboot-scripts/-/jobs/artifacts/master/raw/common.scr?job=build  
https://gitlab.com/switchroot/bootstack/switch-uboot-scripts/-/jobs/artifacts/master/raw/sd.scr?job=build  

Go to `out/target/product/[device name]` (ex. `out/target/product/icosa_sr`) in your lineage source directory and copy `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

Copy `boot.img` and `install/tegra210-icosa.dtb` from that same directory to `/switchroot/install` on your SD card.

Download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and copy it to `/switchroot/install`.

Launch Hekate and navigate to `Tools` -> `Arch bit • RCM • Touch • Partitions` -> `Partition SD Card`.

You can use the `Android ` slider to choose how much storage you want to give to Android, then press `Next Step`, then `Start`.

Once that finishes press `Flash Android`, then `Continue`.

Press `Continue` again and it should reboot to TWRP.

If it doesn't reboot to TWRP, hold the power button for 12 seconds, boot into hekate again, then select the Android config while holding VOL-UP and hold VOL-UP until it gets into TWRP.

In TWRP tap mount, then check any/every partition you can, if some/all cannot be selected, just continue.

Now go back and press `Install` then navigate to `external_sd` then to wherever you put `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` and tap on it.

Swipe to confirm and it should install the rest of Android.

Now just reboot, load hekate again, select the Android config, and hope it boots.


# Updating:
```bash
.repo/local_manifests/snack.sh -y
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G
```

Run one of the following commands:

`lunch lineage_icosa_sr-userdebug`: Standard Android with Nvidia apps.  
`lunch lineage_icosa_tv_sr-userdebug`: Android TV with Nvidia apps.  
NOTE: Downloadable Nvidia Shield exclusive games/ports (such as Half Life 2) do not currently work.  

Now run:
```bash
make bacon
```

Go to `out/target/product/[device name]` (ex. `out/target/product/icosa_sr`) in your lineage source directory and copy `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

If TWRP has been updated since you last flashed, download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and put it in `switchroot/install` on your SD card and in hekate navigate to `Tools` -> `Arch bit • RCM • Touch • Partitions` -> `Partition SD Card` then `Flash Android`

Boot into TWRP by holding the volume up button while booting Android, once in TWRP navigate to `external_sd` and flash the zip you placed on your SD card earlier.
